Summary
=======
This is the DuMuX module containing the code for reproducing the results of

* Michalkowski, Veyskarami, Bringedal, Helmig & Schleper (2022): [*Two-phase flow dynamics at the interface between GDL and gas distributor channel using a pore-network model*]


Installation
============

__Dependencies__
This module [requires](https://dune-project.org/doc/installation/)
* a standard c++17 compliant compiler (e.g. gcc or clang)
* CMake
* pkg-config
* MPI (e.g., OpenMPI)
* Suitesparse (install with `sudo apt-get install libsuitesparse-dev` on Debian/Ubuntu systems)

The easiest way to install this module is to create a new folder and to execute the file
[installMichalkowski2022a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/michalkowski2022a/-/blob/pub/DropletsPNMMultiTube/installMichalkowski2022a.sh)
in this folder.

```bash
mkdir -p Michalkowski2022a && cd Michalkowski2022a
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/michalkowski2022a/-/raw/pub/DropletsPNMMultiTube/installMichalkowski2022a.sh && chmod +x installMichalkowski2022a.sh
sh ./installMichalkowski2022a.sh
```
```
You may use [ParaView](https://www.paraview.org/) to visualize the results by opening the two `.pvd` files. The `.txt` files contain additional data such as the evaporation rate over time.
```

After the script has run successfully, you may build the program executables
```
cd michalkowski2022a/build-cmake/appl/2p_SingleTubeDroplet
make appl_singleTube
cd ../2p_MultiTubeDroplets
make appl_pnm_2p_droplets
cd ../2p_GDLDroplets
make appl_GDL_2p_droplets
```

and run them in the specific folders, e.g., with

```
./appl_singleTube params.input
./appl_pnm_2p_droplets params-MultiTubeDroplets3.input
./appl_GDL_2p_droplets params-GDL-pub.input
```

Single-Tube Example
============
The first example (2p_SingleTubeDroplet) presents the application of the drop condition in a small system where only one droplet can be formed at the interface. It allows to investigate the behavior of a single droplet during formation, growth and detachment (due to external gas flow drag force).

Multi-Tube Example
============
In the second example (2p_MultiTubeDroplets), the drop condition is applied to a more advanced network where multiple droplets can be formed at the interface. Different setups can be chosen by the different input files. The setups differ in the number of interface throats, where dropltes can form. With this examples the interactive behavior of multiple droplets during formation, growth and detachment (due to external gas flow drag force) can be analysed.

GDL Example
============
The last example (2p_GDLDroplets) presents the pplication of the drop interface condition on a realistic GDL representation using a pore network. The network has been generated based on an artificially generated fiber structure (generated using Math2Market GeoDict) and extracted using the open source project PoreSpy.
