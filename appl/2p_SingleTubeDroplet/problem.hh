// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A test problem for the one-phase pore network model.
 */
#ifndef DUMUX_PNM2P_PROBLEM_HH
#define DUMUX_PNM2P_PROBLEM_HH

#include <ctime>
#include <iostream>

#include <dune/foamgrid/foamgrid.hh>

#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porenetwork/2p/model.hh>
#include <dumux/common/boundarytypes.hh>

// spatial params
#include "files/spatialparams.hh"

#include <dumux/material/components/simpleh2o.hh>
#include <dumux/porenetwork/common/utilities.hh>
#include "files/h2oair.hh"

#include <dumux/io/gnuplotinterface.hh>

#ifndef ISOTHERMAL
#define ISOTHERMAL 1
#endif

namespace Dumux
{
template <class TypeTag>
class DrainageProblem;

namespace Properties
{
// Create new type tags
namespace TTag {
#if ISOTHERMAL
struct DrainageProblem { using InheritsFrom = std::tuple<PNMTwoP>; };
#else
struct DrainageProblem { using InheritsFrom = std::tuple<PNMTwoPNI>; };
#endif
} // end namespace TTag

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::DrainageProblem> { using type = Dumux::DrainageProblem<TypeTag>; };

template<class TypeTag>
struct FluidSystem<TypeTag, TTag::DrainageProblem>
  {
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = Dumux::FluidSystems::H2OAir<Scalar, Dumux::Components::SimpleH2O<Scalar>>;
  };

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::DrainageProblem> { using type = Dune::FoamGrid<1, 3>; };

// Set formulation (pw and sn or pn and sw)
template<class TypeTag>
struct Formulation<TypeTag, TTag::DrainageProblem>
{ static constexpr auto value = TwoPFormulation::p1s0; }; //p1s0 is pnsw

// Set the spatial params  
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::DrainageProblem>
{
private:
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using LocalRules = Dumux::PoreNetwork::FluidMatrix::MultiShapeTwoPLocalRules<Scalar>;

public:
    using type = PNMDropletTwoPSpatialParams<GridGeometry, Scalar, LocalRules>;

};
}
template <class TypeTag>
class DrainageProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using ElementVolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::LocalView;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;

    // copy some indices for convenience
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using Labels = GetPropType<TypeTag, Properties::Labels>;
    enum {
        // grid and world dimension
        dim = GridView::dimension,
        dimworld = GridView::dimensionworld,

        // primary variable indices
        pnIdx = Indices::pressureIdx,
        swIdx = Indices::saturationIdx,


        // phase indices
        wPhaseIdx = FluidSystem::phase0Idx, //gas
        nPhaseIdx = FluidSystem::phase1Idx, //liquid

#if !ISOTHERMAL
        temperatureIdx = Indices::temperatureIdx,
        energyEqIdx = Indices::energyEqIdx,
#endif
    

    };

    using Element = typename GridView::template Codim<0>::Entity;
    using Vertex = typename GridView::template Codim<dim>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;


public:
    template<class SpatialParams>
    DrainageProblem(std::shared_ptr<const GridGeometry> gridGeometry, std::shared_ptr<SpatialParams> spatialParams)
    : ParentType(gridGeometry, spatialParams), eps_(1e-15)
    {
        verbose_ = getParam<bool>("Problem.Verbose", false);
        VtpOutputFrequency_ = getParam<int>("Problem.VtpOutputFrequency");
        pnOutlet_ = getParam<Scalar>("Problem.pnOutlet");
        swInlet_ = getParam<Scalar>("Problem.swInlet");
        sigma_ = getParam<Scalar>("SpatialParams.SurfaceTension", 0.0725);
        channelHeight_ = getParam<Scalar>("Problem.channelHeight");
        gasVelocity_ = getParam<Scalar>("Problem.gasVelocityChannel");
        advContactAngle_ = getParam<Scalar>("SpatialParams.advancingContactAngle");
        resContactAngle_ = getParam<Scalar>("SpatialParams.receedingContactAngle");
        eqContactAngle_ = getParam<Scalar>("SpatialParams.equilibriumContactAngle");
        sourceFluxH2O_ = getParam<Scalar>("Problem.sourceFluxH2O");

        plotName_ = getParam<std::string>("Plot.Name");
    }

    bool shouldWriteOutput(const int timeStepIndex, const GridVariables& gridVariables) const //define output
    {
        if(VtpOutputFrequency_ < 0)
            return true;
        if(VtpOutputFrequency_ == 0)
            return ( timeStepIndex == 0 || gridVariables.gridFluxVarsCache().invasionState().hasChanged());
        else
            return ( timeStepIndex % VtpOutputFrequency_ == 0 || gridVariables.gridFluxVarsCache().invasionState().hasChanged());
    }

    void postTimeStep(const int timeStepIndex, const GridVariables& gridVariables, SolutionVector& sol, Scalar time)
    {
        const auto& gridView = this->gridGeometry().gridView();
        
        std::vector<double> dropVolume;
        std::vector<double> dropRadiusClean;
        std::vector<double> dRadius;
        dropVolume.resize(this->gridGeometry().numScv(),-1);
        dRadius.resize(this->gridGeometry().numScv(),0);
        
        auto innerBCpores = boundaryPores(gridVariables, sol);
	    
        for (const auto& element : elements(gridView))
        {
	        auto fvGeometry = localView(this->gridGeometry());
	        fvGeometry.bindElement(element);

	        auto elemVolVars = localView(gridVariables.curGridVolVars());
            elemVolVars.bindElement(element, fvGeometry, sol);

            for (auto&& scv : scvs(fvGeometry))
            {
                if (std::find(innerBCpores.begin(), innerBCpores.end(), scv.dofIndex()) != innerBCpores.end()){
                    auto elemSol = elementSolution(element, sol, this->gridGeometry());
                    const Scalar contactAngle = this->spatialParams().contactAngle(element,scv,elemSol);
                    if (elemVolVars[scv].saturation(nPhaseIdx) >= eps_)
                    {
                        const Scalar poreVolume = this->gridGeometry().poreVolume(scv.dofIndex());
                        const Scalar dropVolume = elemVolVars[scv].saturation(nPhaseIdx)*poreVolume;
                        setDropRadius(scv, std::cbrt(dropVolume*3/M_PI/((2-0.7660444431189779)*(1+0.7660444431189779)*(1+0.7660444431189779))));
                        std::cout << "saturation larger than eps: " << getDropRadius(scv) << std::endl;
                        std::cout << "non-wetting phase pressure: " <<  elemVolVars[scv].pressure(nPhaseIdx) << std::endl;
                       
                    }
                    else
                    {
                        setDropRadius(scv, 0); 
                        std::cout << "saturation smaller than eps: " << getDropRadius(scv) << std::endl; 
                    }                   
                    dRadius[scv.dofIndex()] = getDropRadius(scv);
                    Scalar dropHeight = getDropRadius(scv)*(1 - std::cos(M_PI - contactAngle));
                    dropVolume[scv.dofIndex()] = 1/3*M_PI*dropHeight*dropHeight *(3*getDropRadius(scv)-dropHeight);          
                }
            }
        }
	    x_.push_back(time); // in seconds

        int idx = 0;
        int idx2 = 0;
        for (std::vector<double>::const_iterator i = dropVolume.begin(); i != dropVolume.end(); ++i){
            if (*i > -1.0){
                dropRadiusClean.push_back(dRadius.at(idx));
                idx2 +=1;
            }
            idx+=1;
        }
        idx = 0;
        int numberBCPores = count_if (dropVolume.begin(), dropVolume.end(), [](double i) {return i > -1.0;});
        y_.resize(numberBCPores, std::vector<double>(1));
        for(int i = 0; i < numberBCPores; ++i) {
            (y_)[idx].push_back(dropRadiusClean.at(idx));
            idx += 1;
        }
    
        gnuplot_.resetPlot();
        gnuplot_.setXRange(0, std::max(time, 1e-10));
        gnuplot_.setYRange(0, 5e-3);
        gnuplot_.setXlabel("time");
        gnuplot_.setYlabel("drop radius");

        for(int i = 0; i < numberBCPores; ++i) {
            gnuplot_.addDataSetToPlot(x_, (y_)[i], "pore"+std::to_string(i)+ "-" + plotName_ + ".dat");
        }

        gnuplot_.plot("dropVolume");
    }



#if ISOTHERMAL
    /*!
     * \brief Return the temperature within the domain in [K].
     *
     */
    Scalar temperature() const
    { return 273.15 + 80; } // 10°C

#endif
     /*!
     * \name Boundary conditions
     */
    //! Specifies which kind of boundary condition should be used for
    //! which equation for a finite volume on the boundary.
    BoundaryTypes boundaryTypes(const Element &element, const SubControlVolume &scv) const
    {
        BoundaryTypes bcTypes;
        if (isOutletPore_(scv))
           bcTypes.setAllDirichlet();
        else // neuman for the remaining boundaries
           bcTypes.setAllNeumann();
#if !ISOTHERMAL
        bcTypes.setDirichlet(temperatureIdx);
#endif
        return bcTypes;
    }

    /*!
    * \brief Evaluate the boundary conditions for a dirichlet
    *        control volume.
    *
    * \param values The dirichlet values for the primary variables
    * \param vertex The vertex (pore body) for which the condition is evaluated
    *
    * For this method, the \a values parameter stores primary variables.
    */
    PrimaryVariables dirichlet(const Element &element,
                               const SubControlVolume &scv) const
    {
        PrimaryVariables values(0.0);

        if(isOutletPore_(scv)){
            values[pnIdx] = pnOutlet_;
            values[swIdx] = 1.0;
        }
#if !ISOTHERMAL
        if(isInletPore_(scv)){
            values[temperatureIdx] = 273.15 + 15;
        }else{
            values[temperatureIdx] = 273.15 + 10;
        }
#endif
        return values;
    }


    // \}

    /*!
     * \name Volume terms
     */
    // \{
    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * This is the method for the case where the source term is
     * potentially solution dependent and requires some quantities that
     * are specific to the fully-implicit method.
     *
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param elemVolVars All volume variables for the element
     * \param scv The sub control volume
     *
     * For this method, the return parameter stores the conserved quantity rate
     * generated or annihilate per volume unit. Positive values mean
     * that the conserved quantity is created, negative ones mean that it vanishes.
     * E.g. for the mass balance that would be a mass rate in \f$ [ kg / (m^3 \cdot s)] \f$.
     */
    PrimaryVariables source(const Element &element,
                            const FVElementGeometry& fvGeometry,
                            const ElementVolumeVariables& elemVolVars,
                            const SubControlVolume &scv) const
    {
        PrimaryVariables values(0.0);
        if (this->gridGeometry().poreLabel(scv.dofIndex()) ==Labels::source)
        {
            std::cout << "saturation in droplet pore: " << elemVolVars[scv].saturation(nPhaseIdx) << std::endl;
            std::cout << "capillary pressure in droplet pore: " << elemVolVars[scv].pressure(nPhaseIdx) -  elemVolVars[scv].pressure(wPhaseIdx) << std::endl;
            if (elemVolVars[scv].saturation(nPhaseIdx) >= eps_)
            {
                GetPropType<TypeTag, Properties::FluidState> fluidState;
                fluidState.setTemperature(temperature());
                fluidState.setPressure(wPhaseIdx, pnOutlet_);
                const Scalar viscosity = 0.00002007;
                const Scalar nDensity = FluidSystem::density(fluidState, nPhaseIdx);
                const Scalar dropHeight = getDropRadius(scv) *(1 - std::cos(advContactAngle_));
                
                //check detachment criterium and detach/delete drop
                // pressure drop based on analytic solution for rectangular duct
                const Scalar pressureForce = 24*viscosity *(channelHeight_/2)*(channelHeight_/2)*gasVelocity_*dropHeight*dropHeight/((channelHeight_/2-dropHeight/2)*(channelHeight_/2-dropHeight/2)*(channelHeight_/2-dropHeight/2)*(1-std::cos(advContactAngle_))*(1-std::cos(advContactAngle_)));
                //shear force based on parabolic flow profile between parallel plates
                const Scalar topShearForce = 3*viscosity*channelHeight_/2*4*getDropRadius(scv)*getDropRadius(scv)/((channelHeight_/2-dropHeight/2)*(channelHeight_/2-dropHeight/2)) *gasVelocity_;
                //side shear force based on integrating shear stress along droplet height
                const Scalar sideShearForce = 12*viscosity*getDropRadius(scv)*dropHeight/channelHeight_*(2-dropHeight/channelHeight_)*gasVelocity_;
                // drag force results from the sum of pressure and shear forces
                const Scalar dragForce = pressureForce + topShearForce + 2*sideShearForce;
                // compute F_ret - max. retention force based on advancing and receeding contact angle (following Kumbur et al. 2006)
                const Scalar contactAngleHysteresis = advContactAngle_ - resContactAngle_;
                const Scalar radiusCA = getDropRadius(scv)*std::sin(M_PI-eqContactAngle_);
                const Scalar retentionForce = - sigma_*radiusCA *M_PI* ((std::sin(contactAngleHysteresis - advContactAngle_)-std::sin(advContactAngle_))/(contactAngleHysteresis - M_PI)+ (std::sin(contactAngleHysteresis - advContactAngle_)-std::sin(advContactAngle_))/(contactAngleHysteresis + M_PI));
                
                std::cout << "pressure force: "<< pressureForce << std::endl;
                std::cout << "top shear force: "<< topShearForce << std::endl;
                std::cout << "side shear force: "<< sideShearForce << std::endl;
                std::cout << "retention force: "<< retentionForce << std::endl;

                // compare forces
                if (std::abs(dragForce) > std::abs(retentionForce))
                {
                    const Scalar poreVolume = this->gridGeometry().poreVolume(scv.dofIndex());
                    values[nPhaseIdx] -= elemVolVars[scv].saturation(nPhaseIdx)*nDensity*1/this->getTimeStepSize()*1/poreVolume;
                    std::cout << "droplet detached at: " << scv.dofIndex() << ", drop radius was: "<<  getDropRadius(scv) << std::endl;
                }
            }
        }
        else if(this->gridGeometry().poreLabel(scv.dofIndex()) ==Labels::inlet)
        {
            values[nPhaseIdx] += sourceFluxH2O_/(this->gridGeometry().poreVolume(scv.dofIndex()));
        }
        return values;
    }
    // \}

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * For this method, the \a priVars parameter stores primary
     * variables.
     */
    PrimaryVariables initial(const Vertex& vertex) const
    {
        PrimaryVariables values(0.0);

        values[pnIdx] = pnOutlet_;

        //get global index of pore
        const auto dofIdxGlobal = this->gridGeometry().vertexMapper().index(vertex);
        if(isInletPore_(dofIdxGlobal))
            values[swIdx] = swInlet_;
	    else if (isSourcePore_(dofIdxGlobal))
	        values[swIdx] = 1.0;
        else
            values[swIdx] = swInlet_;

#if !ISOTHERMAL
        values[temperatureIdx] = 273.15 + 10;
#endif
        return values;
    }

    void initDropRadius()
    {
        dropRadius.resize(this->gridGeometry().numDofs(),0);
    }

    void setDropRadius(const SubControlVolume &scv, Scalar radius) const
    {
        dropRadius[scv.dofIndex()] = radius;
    }

    auto getDropRadius(const SubControlVolume &scv) const
    {
        return dropRadius[scv.dofIndex()];
    }

    void setTimeStepSize(Scalar timeStepSize_) const
    {
        delta_t = timeStepSize_;
    }

    Scalar getTimeStepSize() const
    {
        return delta_t;
    }

    auto boundaryPores (const GridVariables& gridVariables, SolutionVector& sol) const
    {
        std::vector<double> boundaryPores;

        const auto& gridView = this->gridGeometry().gridView();

        for (const auto& element : elements(gridView))
        {
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);

            auto elemVolVars = localView(gridVariables.curGridVolVars());
            elemVolVars.bindElement(element, fvGeometry, sol);
            
            
            for (auto&& scv : scvs(fvGeometry))
            {
                if (this->gridGeometry().poreLabel(scv.dofIndex()) ==Labels::source)
                    boundaryPores.push_back(scv.dofIndex());
            }
        }
        sort( boundaryPores.begin(), boundaryPores.end() );
        boundaryPores.erase( unique( boundaryPores.begin(), boundaryPores.end() ), boundaryPores.end() );

        return boundaryPores; 
    }
        
    /*!
     * \brief Evaluate the initial invasion state of a pore throat
     *
     * Returns true for a invaded throat and false elsewise.
     */
    bool initialInvasionState(const Element &element) const
    { return false; }

    const bool verbose() const
    { return verbose_; }

    bool simulationFinished() const
    { return false ; }


private:

    bool isInletPore_(const SubControlVolume& scv) const
    {
        return isInletPore_(scv.dofIndex());
    }

    bool isInletPore_(const std::size_t dofIdxGlobal) const
    {
        return this->gridGeometry().poreLabel(dofIdxGlobal) ==Labels::inlet;
    }
    
    bool isOutletPore_(const SubControlVolume& scv) const
    {
        return this->gridGeometry().poreLabel(scv.dofIndex()) ==Labels::outlet;
    }
    
     bool isSourcePore_(const SubControlVolume& scv) const
    {
        return this->gridGeometry().poreLabel(scv.dofIndex()) ==Labels::source;
    }
    bool isSourcePore_(const std::size_t dofIdxGlobal) const
    {
        return this->gridGeometry().poreLabel(dofIdxGlobal) ==Labels::source;
    }
   
    
    Scalar eps_;
    bool verbose_;
    int VtpOutputFrequency_;
    Scalar pnOutlet_;
    Scalar swInlet_;
    Scalar sigma_;
    Scalar p_gas_;
    Scalar sourceFluxH2O_;
    Scalar channelHeight_;
    Scalar gasVelocity_;
    Scalar advContactAngle_ ;
    Scalar resContactAngle_ ;
    Scalar eqContactAngle_ ;
    mutable std::vector<int> marker;
    mutable std::vector<double> dropRadius;

    Dumux::GnuplotInterface<double> gnuplot_;

    std::string plotName_;

    std::vector<double> x_;
    std::vector<std::vector<double>> y_;

    std::vector<Scalar> contactAnglePores_;
    std::vector<Scalar> contactAngleThroats_;
    std::vector<Scalar> droplet_;
    std::vector<Scalar> invasionState_;
    mutable Scalar delta_t;
};
} //end namespace

#endif
