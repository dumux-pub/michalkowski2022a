// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup PNMTwoP
 * \brief Definition of the spatial parameters for the pnm two phase
 *        problem with different wetting properties.
 */
#ifndef DUMUX_PNM_DROPLET_TWOP_SPATIAL_PARAMS_HH
#define DUMUX_PNM_DROPLET_TWOP_SPATIAL_PARAMS_HH

#include <dumux/material/spatialparams/porenetwork/porenetwork2p.hh>
#include <dumux/material/spatialparams/porenetwork/porenetworkbase.hh>
#include <dumux/material/fluidmatrixinteractions/porenetwork/throat/thresholdcapillarypressures.hh>

namespace Dumux {


template<class GridGeometry, class Scalar, class MaterialLawT>
class PNMDropletTwoPSpatialParams
: public PoreNetwork::TwoPDefaultSpatialParams<GridGeometry, Scalar, MaterialLawT>
{
    using ParentType = PoreNetwork::TwoPDefaultSpatialParams<GridGeometry, Scalar, MaterialLawT>;
    using GridView = typename GridGeometry::GridView;
    using SubControlVolume = typename GridGeometry::SubControlVolume;
    using Element = typename GridView::template Codim<0>::Entity;

    static const int dim = GridView::dimension;
    static const int dimWorld = GridView::dimensionworld;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

public:

    using MaterialLaw = MaterialLawT;

    PNMDropletTwoPSpatialParams(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry)
    {
        if (!gridGeometry->useSameGeometryForAllPores() && !MaterialLawT::supportsMultipleGeometries())
            DUNE_THROW(Dune::InvalidStateException, "Your MaterialLaw does not support multiple pore body shapes.");

    }


    template<class GlobalPosition>
   Scalar porosityAtPos(const GlobalPosition& globalPos) const
   { return 1.0; }
};
} // end namespace Dumux
#endif
